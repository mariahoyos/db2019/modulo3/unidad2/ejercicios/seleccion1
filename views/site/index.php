<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Selección 1</h1>

        <p class="lead">Módulo 3 - Unidad 2</p>
    </div>

    <div class="body-content">

        <div class="row">
            <!--  
            botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!--  
            fin botón de consulta
            -->
            <!-- inicio consulta 2 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 2 -->
            <!-- inicio consulta 3 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 3 -->
            <!-- inicio consulta 4 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 4 -->
            <!-- inicio consulta 5 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 5 -->
            <!-- inicio consulta 6 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 6</h3>
                        <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 6 -->
            <!-- inicio consulta 7 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 7</h3>
                        <p>Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 7 -->
            
            <!-- inicio consulta 8 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 8</h3>
                        <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 8 -->
            
            <!-- inicio consulta 9 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los puertos cuya altura sea mayor de 1500</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 9 -->
            
            <!-- inicio consulta 10 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 10</h3>
                        <p>lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 10 -->
            
            <!-- inicio consulta 11 -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 11</h3>
                        <p>lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO', ['site/consulta11'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- fin consulta 11 -->
        </div>

    </div>
</div>
