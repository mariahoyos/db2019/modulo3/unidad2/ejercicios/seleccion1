<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Lleva;
use app\models\Puerto;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionCrud(){
        return $this->render("gestion");
    }
    
    public function actionConsulta1a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
        
    }
    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand('select count(distinct edad) from ciclista')
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT edad FROM ciclista',
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta2a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
        
    }
    
    public function actionConsulta2(){
        //mediante DAO
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct edad) FROM ciclista WHERE nomequipo = 'Artiach'")
                ->queryScalar();
        
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    
    public function actionConsulta3a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("edad")
                    ->distinct()
                    ->where("nomequipo='Artiach'")
                    ->orWhere("nomequipo='Amore Vita'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
        
    }
    
    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
            'totalCount' => $numero,
            'pagination' =>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render('resultado',[
            "resultados" => $dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
            
        
        
    }
    
    public function actionConsulta4a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("dorsal")
                    ->distinct()
                    ->where("edad<24")
                    ->orWhere("edad>30"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad<24 OR edad>30",
        ]);
        
    }
    
    public function actionConsulta4(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE edad<24 OR edad>30")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal FROM ciclista WHERE edad<24 OR edad>30",
            'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad<24 OR edad>30",
        ]);
    }
    
    public function actionConsulta5a(){
        //mediante active record
            $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                    ->select("dorsal")
                    ->distinct()
                    ->where("edad between 28 and 32")
                    ->andWhere("nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto')",
        ]);
         
    }
    
    public function actionConsulta5(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto')")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql' => "SELECT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto')",
            'totalCount' =>$numero,
            'pagination'=>[
                'pageSize'=>5
            ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE (edad BETWEEN 28 AND 32) AND (nomequipo = 'Banesto')",
        ]);
    }
    
    public function actionConsulta6a(){
    //por terminar    
        $dataProvider = new ActiveDataProvider([
           'query'  => Ciclista::find()
                ->select("nombre")
                ->distinct()
                ->where("CHAR_LENGTH(nombre)>8"),
            'pagination'=>[
                'pageSize'=>5,
            ]
        ]);
        
        return $this->render('resultado',[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM  ciclista WHERE CHAR_LENGTH(nombre)>8)",
        ]);
        
    }
    
    public function actionConsulta6(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(distinct nombre) FROM  ciclista WHERE CHAR_LENGTH(nombre)>8")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT nombre FROM  ciclista WHERE CHAR_LENGTH(nombre)>8",
           'totalCount' => $numero,
           'pagination'=>[
               'pageSize'=>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM  ciclista WHERE CHAR_LENGTH(nombre)>8)",
        ]);
        
    }
    
    public function actionConsulta7a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->select("upper(nombre) nombre,dorsal")
                ->distinct()
        ]);
        
        
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT UPPER(nombre) nombre, dorsal FROM ciclista",
            
        ]);
     
    }
    
    public function actionConsulta7(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM ciclista")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT UCASE(nombre)nombre, dorsal FROM ciclista",
           'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nombre','dorsal'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT UPPER(nombre) nombre, dorsal FROM ciclista",
        ]);
        
    }
    
    public function actionConsulta8a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()
                ->select('dorsal')
                ->distinct()
                ->where("código='MGE'")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
            
        ]);
     
    }
    
    public function actionConsulta8(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM lleva WHERE código = 'MGE'")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
           'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
        ]);
        
    }
    
    public function actionConsulta9a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select('nompuerto')
                ->distinct()
                ->where("altura>1500")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura>1500",
            
        ]);
     
    }
    
    public function actionConsulta9(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT nompuerto FROM puerto WHERE altura>1500",
           'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT nompuerto FROM puerto WHERE altura>1500",
        ]);
        
    }
    
    public function actionConsulta10a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select('dorsal')
                ->distinct()
                ->where("pendiente>8")
                ->orWhere("altura BETWEEN 1800 AND 3000")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
            
        ]);
     
    }
    
    public function actionConsulta10(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
           'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
        
    }
    
    public function actionConsulta11a(){
        
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()
                ->select('dorsal')
                ->distinct()
                ->where("pendiente>8")
                ->andWhere("altura BETWEEN 1800 AND 3000")
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
            
        ]);
     
    }
    
    public function actionConsulta11(){
        
        $numero = Yii::$app->db
                ->createCommand("SELECT count(*) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
           'sql' => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
           'totalCount' => $numero,
           'pagination' => [
               'pageSize' =>5
           ]
        ]);
        
        return $this->render('resultado', [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
        ]);
        
    }
    
}
